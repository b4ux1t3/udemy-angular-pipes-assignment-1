# Anular Pipes Assignment 1

This repo is an implementation of one of the course assignments for Maximillian Schwarzmüller's Angular course on Udemy.

To see the app running, check it out, [hosted right here on GitLab](https://b4ux1t3.gitlab.io/udemy-angular-pipes-assignment-1).
