import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: string): string {
    const myStack = new Stack();

    for(const char of value) {
      myStack.push(char);
    }
    let returnString: string = '';
    for(let i = 0; i < value.length; i++) {
      returnString += myStack.pop();
    }
    return returnString;
  }

}
export class Stack<T> {
  private nodes: T[];

  constructor(initialValue?: T){
    this.nodes = [];
    if (initialValue !== null ) this.nodes.push(initialValue);
  }

  push(newNode: T): void {
    this.nodes.push(newNode);
  }

  pop(): T {
    return this.nodes.pop();
  }
}


