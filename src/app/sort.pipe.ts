import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform<T>(values: T[], sortKey: string): T[] {
    if (values.length === 0) return values;

    return values.sort((a: T, b: T) => a[sortKey].localeCompare(b[sortKey]));
  }

}
export interface Locale {
  localeCompare(): number;
}
